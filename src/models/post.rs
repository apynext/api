use sqlx::PgPool;
use time::OffsetDateTime;
use tracing::warn;

use crate::utils::app_error::AppError;

use super::account::AccountPermission;
use serde::Serialize;

pub struct Post {
    pub id: i64,
    pub title: String,
    pub content: String,
    pub author: PublicPostAuthor,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
}

/// For public posts
#[derive(Serialize)]
pub struct PublicPost {
    pub id: i64,
    pub author: PublicPostAuthor,
    pub title: String,
    pub content: String,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
}

/// For new posts notifications
#[derive(Serialize)]
pub struct NotificationPost {
    pub id: i64,
    pub author: PublicPostAuthor,
    pub title: String,
    pub created_at: OffsetDateTime,
}

#[derive(Serialize)]
pub struct PublicPostAuthor {
    pub id: i64,
    pub username: String,
    pub permission: AccountPermission,
}

impl Post {
    pub async fn insert_and_get_notification_post(
        pool: &PgPool,
        author_id: i64,
        title: &str,
        content: &str,
    ) -> Result<NotificationPost, AppError> {
        struct PostWithAuthorWrong {
            id: i64,
            title: String,
            created_at: OffsetDateTime,
            author_id: i64,
            author_username: String,
            author_permission: AccountPermission,
        }

        let post = match sqlx::query_file_as!(
            PostWithAuthorWrong,
            "./src/queries/insert_post.sql",
            author_id,
            title,
            content,
        )
        .fetch_one(pool)
        .await
        {
            Ok(post) => post,
            Err(e) => {
                warn!("Error inserting post with author {} : {e}", author_id);
                return Err(AppError::internal_server_error());
            }
        };

        let post = NotificationPost {
            id: post.id,
            title: post.title,
            author: PublicPostAuthor {
                id: post.author_id,
                username: post.author_username,
                permission: post.author_permission,
            },
            created_at: post.created_at,
        };

        Ok(post)
    }

    pub async fn get_posts(
        pool: &PgPool,
        limit: i64,
        offset: i64,
    ) -> Result<(Vec<PublicPost>, i64), AppError> {
        let mut transaction = pool.begin().await.map_err(|e| {
            warn!("Error while creating a transaction : {e}");
            AppError::internal_server_error()
        })?;
        struct PublicPostWithAuthorWrong {
            pub id: i64,
            pub title: String,
            pub content: String,
            pub created_at: OffsetDateTime,
            pub updated_at: OffsetDateTime,
            pub author_id: i64,
            pub author_username: String,
            pub author_permission: AccountPermission,
        }

        let posts = sqlx::query_file_as!(
            PublicPostWithAuthorWrong,
            "./src/queries/select_posts.sql",
            limit,
            offset
        )
        .fetch_all(&mut *transaction)
        .await
        .map_err(|e| {
            warn!("Error getting posts : {e}");
            AppError::internal_server_error()
        })?;

        struct Count {
            total: i64,
        }

        let count = sqlx::query_file_as!(Count, "src/queries/select_count_from_posts.sql")
            .fetch_one(&mut *transaction)
            .await
            .map_err(|e| {
                warn!("Error getting posts count : {e}");
                AppError::internal_server_error()
            })?;

        let count = count.total;

        let posts: Vec<PublicPost> = posts
            .into_iter()
            .map(|post| PublicPost {
                id: post.id,
                title: post.title,
                content: post.content,
                author: PublicPostAuthor {
                    id: post.author_id,
                    username: post.author_username,
                    permission: post.author_permission,
                },
                created_at: post.created_at,
                updated_at: post.updated_at,
            })
            .collect();

        Ok((posts, count))
    }
}
