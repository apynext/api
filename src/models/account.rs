use sqlx::{Executor, PgPool, Postgres};
use time::OffsetDateTime;
use tracing::warn;

use crate::utils::app_error::AppError;

pub struct Account {
    pub id: i64,
    pub username: String,
    pub email: String,
    pub password: String,
    pub birthdate: OffsetDateTime,
    pub dark_mode: bool,
    pub biography: String,
    pub token: String,
    pub is_male: Option<bool>,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
    pub email_verified: bool,
    pub is_banned: bool,
    pub permission: i64,
}

impl Account {
    pub async fn check_if_email_already_exists<'c>(
        pool: impl Executor<'c, Database = Postgres>,
        email: &str,
    ) -> Result<(), AppError> {
        let result = sqlx::query_file!(
            "./src/queries/select_count_of_accounts_with_email.sql",
            email
        )
        .fetch_optional(pool)
        .await
        .map_err(|e| {
            warn!("Error checking if the email `{email}` exists in the database : {e}");
            AppError::internal_server_error()
        })?;

        if result.is_some() {
            warn!("Email `{email}` already exists in the database");
            return Err(AppError::forbidden_error(Some("Email déjà utilisé.")));
        };
        Ok(())
    }

    pub async fn check_if_username_already_exists<'c>(
        pool: impl Executor<'c, Database = Postgres>,
        username: &str,
    ) -> Result<(), AppError> {
        let result = sqlx::query_file!(
            "./src/queries/select_count_of_accounts_with_username.sql",
            username
        )
        .fetch_optional(pool)
        .await
        .map_err(|e| {
            warn!(
                "Error checking if the username `{username}` already exists in the database : {e}"
            );
            AppError::internal_server_error()
        })?;

        if result.is_some() {
            warn!("Username `{username}` already exists in the database",);
            return Err(AppError::forbidden_error(Some(
                "Nom d'utilisateur déjà utilisé.",
            )));
        };

        Ok(())
    }

    pub async fn insert<'c>(
        pool: impl Executor<'c, Database = Postgres>,
        username: &str,
        email_confirm_token: &str,
        password: &str,
        birthdate: OffsetDateTime,
        dark_mode: bool,
        biography: &str,
        is_male: Option<bool>,
    ) -> Result<(), AppError> {
        sqlx::query_file!(
            "./src/queries/insert_account.sql",
            username,
            email_confirm_token,
            password,
            birthdate,
            dark_mode,
            biography,
            email_confirm_token,
            is_male,
        )
        .execute(pool)
        .await
        .map_err(|e| {
            warn!("Error inserting account with username `{username}` : {e}");
            AppError::internal_server_error()
        })?;

        Ok(())
    }
}

pub struct PublicAccount {
    pub id: i64,
    pub username: String,
    pub biography: String,
    pub created_at: OffsetDateTime,
    pub permission: AccountPermission,
}

#[derive(serde::Serialize)]
pub enum AccountPermission {
    User = 0,
    Moderator = 1,
    Administrator = 2,
}

impl From<i32> for AccountPermission {
    fn from(value: i32) -> Self {
        match value {
            1 => AccountPermission::Moderator,
            2 => AccountPermission::Administrator,
            _ => AccountPermission::User,
        }
    }
}
