use std::sync::Arc;

use axum::{extract::State, Json};
use chrono::Duration;
use hyper::StatusCode;
use lettre::Address;
use time::OffsetDateTime;
use tracing::warn;

use crate::models::account::Account;
use crate::utils::app_error::AppError;
use crate::utils::register::check_register_infos;
use crate::utils::register::hash_password;
use crate::utils::token::Token;
use crate::FRONT_URL;
use crate::{utils::register::send_html_message, AppState};

#[derive(serde::Deserialize)]
pub struct NewAccount {
    pub username: String,
    pub email: String,
    pub password: String,
    pub birthdate: i64,
    #[serde(default = "r#true")]
    pub dark_mode: bool,
    #[serde(default)]
    pub biography: String,
    pub is_male: Option<bool>,
}

fn r#true() -> bool {
    true
}

pub async fn register_route(
    State(app_state): State<Arc<AppState>>,
    Json(mut register_user): Json<NewAccount>,
) -> Result<StatusCode, AppError> {
    register_user.username = register_user.username.to_lowercase();
    register_user.email = register_user.email.to_lowercase();

    check_register_infos(&register_user)?;

    let email = register_user.email.parse::<Address>().map_err(|e| {
        warn!("Cannot parse email `{}` : {}", register_user.email, e);
        AppError::new(StatusCode::FORBIDDEN, Some("Email invalide."))
    })?;

    register_user.password = hash_password(&register_user.password);

    let birthdate = OffsetDateTime::from_unix_timestamp(register_user.birthdate).map_err(|e| {
        warn!("Invalid birthdate `{}` : {}", register_user.birthdate, e);
        AppError::new(StatusCode::FORBIDDEN, Some("Date de naissance invalide."))
    })?;

    if birthdate.year() < 1900 || birthdate > OffsetDateTime::now_utc() {
        warn!("La date de naissance doit être située entre 1900 et maintenant.");
        return Err(AppError::new(
            StatusCode::FORBIDDEN,
            Some("Date de naissance invalide."),
        ));
    }

    let mut transaction = app_state.pool.begin().await.map_err(|e| {
        warn!("Error creating transaction : {e}");
        AppError::internal_server_error()
    })?;

    //Check if email is already used
    Account::check_if_email_already_exists(&mut *transaction, &register_user.email).await?;

    //Check if username is already used
    Account::check_if_username_already_exists(&mut *transaction, &register_user.username).await?;

    //Generate the email confirmation token
    let email_confirm_token = Token::create(
        register_user.email.clone(),
        Duration::minutes(10),
        &app_state.cipher,
    );

    Account::insert(
        &mut *transaction,
        &register_user.username,
        &email_confirm_token,
        &register_user.password,
        birthdate,
        register_user.dark_mode,
        &register_user.biography,
        register_user.is_male,
    )
    .await?;

    if let Err(e) = transaction.commit().await {
        warn!("Error committing creation : {e}");
        return Err(AppError::internal_server_error());
    };

    let email_confirm_token = urlencoding::encode(&email_confirm_token).to_string();

    send_html_message(
        &app_state.smtp_client,
        "Vérification d'email",
        &format!("<p>Bienvenue <b>@{}</b> ! Un compte a été créé en utilisant cette adresse email, si tu es à l’origine de cette action, clique <a href='{FRONT_URL}{}?token={email_confirm_token}'>ici</a> pour l'activer.\nTu peux également copier-coller le token directement :<div><code>{email_confirm_token}</code></div>.\nSi tu n'es pas à l'origine de cette action, tu peux ignorer cet email.</p>", register_user.username, env!("EMAIL_CONFIRM_ROUTE")),
        email,
    )?;

    Ok(StatusCode::OK)
}
