use crate::{
    extractors::auth_extractor::AuthUser,
    models::post::{Post, PublicPost},
    utils::{app_error::AppError, pagination::PaginationParams},
    AppState,
};
use axum::{
    extract::{Query, State},
    http::HeaderValue,
    Json,
};
use hyper::HeaderMap;
use std::sync::Arc;
use tracing::warn;

pub async fn get_posts_route(
    //TODO perhaps use it
    AuthUser(auth_user): AuthUser,
    Query(pagination_params): Query<PaginationParams>,
    State(app_state): State<Arc<AppState>>,
) -> Result<(HeaderMap, Json<Vec<PublicPost>>), AppError> {
    let mut limit = pagination_params.limit.unwrap_or(10);
    if limit == 0 {
        return Ok((HeaderMap::default(), Json(vec![])));
    }
    if limit.is_negative() {
        limit = 10;
    }

    let mut offset = pagination_params.offset.unwrap_or(0);
    if offset.is_negative() {
        offset = 0;
    }

    let (posts, count) = Post::get_posts(&app_state.pool, limit, offset).await?;

    let count = HeaderValue::from_str(&count.to_string()).map_err(|e| {
        warn!("Error creating the header value : {e}");
        AppError::internal_server_error()
    })?;

    let mut header_map = HeaderMap::new();
    header_map.insert("posts_count", count);

    Ok((header_map, Json(posts)))
}
