use crate::models::post::Post;
use crate::utils::post::check_new_post_data;
use crate::{
    extractors::auth_extractor::AuthUser,
    utils::{
        app_error::AppError,
        real_time_event_management::{EventTracker, RealTimeEvent, WsEvent},
    },
    AppState,
};
use axum::{extract::State, Extension, Json};
use std::sync::Arc;
use tracing::warn;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct NewPost {
    pub title: String,
    pub content: String,
}

pub async fn publish_post_route(
    State(app_state): State<Arc<AppState>>,
    AuthUser(auth_user): AuthUser,
    Extension(event_tracker): Extension<EventTracker>,
    Json(post): Json<NewPost>,
) -> Result<String, AppError> {
    let Some(auth_user) = auth_user else {
        warn!("User not connected");
        return Err(AppError::you_have_to_be_connected_to_perform_this_action_error());
    };

    let title = post.title.trim();
    let content = post.content.trim();

    check_new_post_data(auth_user.id, title, content)?;

    let post =
        Post::insert_and_get_notification_post(&app_state.pool, auth_user.id, title, content)
            .await?;

    let event = WsEvent::new_new_post_notification_event(&post).to_string();

    event_tracker
        .notify(
            RealTimeEvent::NewPostNotification {
                followed_user_id: auth_user.id,
            },
            event,
        )
        .await;

    Ok(format!("/@{}/{}", auth_user.id, post.id))
}
